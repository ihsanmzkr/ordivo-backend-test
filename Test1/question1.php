<?php
function sum_deep($tree, $characters) {
    $totalSum = 0;
    $charactersArr = str_split($characters);
    $level = 1;

    foreach ($tree as $node) {
        $levelSum = 0;
        traverse($node, $level, $charactersArr, $levelSum);
        $totalSum += $levelSum;
    }

    return $totalSum;
}

function traverse($node, $level, $characters, &$levelSum) {
    if (is_array($node)) {
        foreach ($node as $child) {
            traverse($child, $level + 1, $characters, $levelSum);
        }
    } else if (is_string($node)) {
        foreach ($characters as $index => $character) {
            if (strpos($node, $character) !== false) {
                $levelSum += $level * ($index + 1);
            }
        }
    }
}

// Example test cases
$tree_a = ["AB", ["XY"], ["YP"]];
$characters_a = 'Y';
echo sum_deep($tree_a, $characters_a) . "</br>";  // Output: 4

$tree_b = ["", ["", ["XXXXX"]]];
$characters_b = 'X';
echo sum_deep($tree_b, $characters_b) . "</br>";  // Output: 3

$tree_c = ["X"];
$characters_c = 'X';
echo sum_deep($tree_c, $characters_c) . "</br>";  // Output: 1

$tree_d = [""];
$characters_d = 'X';
echo sum_deep($tree_d, $characters_d) . "</br>";  // Output: 0

$tree_e = ["X", ["", ["", ["Y"], ["X"]]], ["X", ["", ["Y"], ["Z"]]]];
$characters_e = 'X';
echo sum_deep($tree_e, $characters_e) . "</br>";  // Output: 7

$tree_f = ["X", [""], ["X"], ["X"], ["Y", [""]], ["X"]];
$characters_f = 'X';
echo sum_deep($tree_f, $characters_f) . "</br>";  // Output: 7
?>