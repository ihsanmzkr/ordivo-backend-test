<?php
function pattern_count($text, $pattern) {
    $textLength = strlen($text);
    $patternLength = strlen($pattern);
    $count = 0;

    if ($patternLength === 0) {
        return 0;
    }

    for ($i = 0; $i <= $textLength - $patternLength; $i++) {
        $match = true;
        for ($j = 0; $j < $patternLength; $j++) {
            if ($text[$i + $j] !== $pattern[$j]) {
                $match = false;
                break;
            }
        }
        if ($match) {
            $count++;
        }
    }

    return $count;
}

// Example usage:
$text1 = "palindrom";
$pattern1 = "ind";
$result1 = pattern_count($text1, $pattern1);
echo $result1;  // Output: 1

$text2 = "abakadabra";
$pattern2 = "ab";
$result2 = pattern_count($text2, $pattern2);
echo $result2;  // Output: 2

$text3 = "hello";
$pattern3 = "";
$result3 = pattern_count($text3, $pattern3);
echo $result3;  // Output: 0

$text4 = "ababab";
$pattern4 = "aba";
$result4 = pattern_count($text4, $pattern4);
echo $result4;  // Output: 2

$text5 = "aaaaaa";
$pattern5 = "aa";
$result5 = pattern_count($text5, $pattern5);
echo $result5;  // Output: 5

$text6 = "hell";
$pattern6 = "hello";
$result6 = pattern_count($text6, $pattern6);
echo $result6;  // Output: 0
