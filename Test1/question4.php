<?php
abstract class Ship {
    protected $name;
    protected $length;
    protected $maxSpeed;

    public function __construct($name, $length, $maxSpeed) {
        $this->name = $name;
        $this->length = $length;
        $this->maxSpeed = $maxSpeed;
    }

    public function getName() {
        return $this->name;
    }

    public function getLength() {
        return $this->length;
    }

    public function getMaxSpeed() {
        return $this->maxSpeed;
    }

    abstract public function sail();
}

class MotorBoat extends Ship {
    private $engineType;

    public function __construct($name, $length, $maxSpeed, $engineType) {
        parent::__construct($name, $length, $maxSpeed);
        $this->engineType = $engineType;
    }

    public function getEngineType() {
        return $this->engineType;
    }

    public function sail() {
        return "Motor boat is sailing.";
    }
}

class Sailboat extends Ship {
    private $sailType;

    public function __construct($name, $length, $maxSpeed, $sailType) {
        parent::__construct($name, $length, $maxSpeed);
        $this->sailType = $sailType;
    }

    public function getSailType() {
        return $this->sailType;
    }

    public function sail() {
        return "Sailboat is sailing.";
    }
}

class Yacht extends Ship {
    private $luxuryLevel;

    public function __construct($name, $length, $maxSpeed, $luxuryLevel) {
        parent::__construct($name, $length, $maxSpeed);
        $this->luxuryLevel = $luxuryLevel;
    }

    public function getLuxuryLevel() {
        return $this->luxuryLevel;
    }

    public function sail() {
        return "Yacht is sailing.";
    }
}

// Example usage:
$motorBoat = new MotorBoat("Motor Boat 1", 10, 40, "Outboard");
echo 'Name : ' . $motorBoat->getName() . '</br>';  // Output: Motor Boat 1
echo 'Length : ' . $motorBoat->getLength() . '</br>';  // Output: 10
echo 'Max Speed : ' . $motorBoat->getMaxSpeed() . '</br>';  // Output: 40
echo 'Type : ' . $motorBoat->getEngineType() . '</br>';  // Output: Outboard
echo 'Status : ' . $motorBoat->sail() . '</br>';  // Output: Motor boat is sailing.
echo '<hr />';

$sailboat = new Sailboat("Sailboat 1", 15, 30, "Sloop");
echo 'Name : ' . $sailboat->getName() . '</br>';  // Output: Sailboat 1
echo 'Length : ' . $sailboat->getLength() . '</br>';  // Output: 15
echo 'Max Speed : ' . $sailboat->getMaxSpeed() . '</br>';  // Output: 30
echo 'Type : ' . $sailboat->getSailType() . '</br>';  // Output: Sloop
echo 'Status : ' . $sailboat->sail() . '</br>';  // Output: Sailboat is sailing.
echo '<hr />';

$yacht = new Yacht("Yacht 1", 20, 50, "High");
echo 'Name : ' . $yacht->getName() . '</br>';  // Output: Yacht 1
echo 'Length : ' . $yacht->getLength() . '</br>';  // Output: 20
echo 'Max Speed : ' . $yacht->getMaxSpeed() . '</br>';  // Output: 50
echo 'Type : ' . $yacht->getLuxuryLevel() . '</br>';  // Output:
echo 'Status : ' . $yacht->sail() . '</br>';
