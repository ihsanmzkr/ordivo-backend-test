<?php
function get_scheme($html) {
    $pattern = '/<(\w+)([^>]*)>/';
    preg_match_all($pattern, $html, $matches);

    $schemes = [];
    foreach ($matches[2] as $attributes) {
        preg_match_all('/\b(?:sc-)(\w+)\b/', $attributes, $schemeMatches);
        foreach ($schemeMatches[1] as $scheme) {
            if (!in_array($scheme, $schemes)) {
                $schemes[] = $scheme;
            }
        }
    }

    return $schemes;
}

// Example usage:
$html1 = "<i sc-root>Hello</i>";
$result1 = get_scheme($html1);
print_r($result1);  // Output: ["root"]

$html2 = "<div><div sc-rootbear title='Oh My'>Hello <i sc-org>World</i></div></div>";
$result2 = get_scheme($html2);
print_r($result2);  // Output: ["rootbear", "org"]
