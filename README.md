## Ordivo Backend Test

### How to run Test 1
Clone the repository, copy folder `Test1` to your webserver folder like htdocs or www and open the file on browser.

### How to run Test 2
This project is requiring to run on `php 8.0`

Clone the repository

Install dependencies
```
composer install
```

Create mongoDB database and give name `ordivotest` for example.

Setup database and queue connection in .env file
```
DB_CONNECTION=mongodb
DB_HOST=127.0.0.1
DB_PORT=27017
DB_DATABASE=ordivotest
DB_USERNAME=
DB_PASSWORD=

QUEUE_CONNECTION=mongodb
```

Run project
```
php artisan serve
```

Run Queue
```
php artisan queue:work
```

Running Unit tests
```
vendor/bin/phpunit
```

Import Postman collection located in folder `Test2` and you can test the API.


Best regards


Ihsan Mudzakir