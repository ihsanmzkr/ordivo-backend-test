<?php

namespace Tests\Feature;

use App\Models\Chart;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChartTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testGetEmptyListProductOnCharts(){
        Chart::truncate();
        $this->json('GET', 'api/chart', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "status" => false,
                "code" => "002",
                "message" => "chart is empty",
                "data" => null
            ]);
    }

    public function testStoreProductNotFoundToChart(){

        $data = ['product_id' => '4214123', 'qty' => '1'];
        $this->json('POST', 'api/chart/store', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "status" => false,
                "code" => "404",
                "message" => "product not found",
                "data" => null
            ]);
    }

    public function testStoreProductToChart(){
        $product = new Product();
        $product->name = 'Test Product';
        $product->price = 1000;
        $product->qty = 111;
        $product->save();
        $productId = $product->id;

        $data = ['product_id' => $productId, 'qty' => '1'];
        $this->json('POST', 'api/chart/store', $data, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                "status",
                "code",
                "message",
                "data" => [
                    "_id",
                    "product_id",
                    "name",
                    "price",
                    "qty",
                    "total",
                    "updated_at",
                    "created_at"
                ]
            ]);
    }

    public function testGetListProductOnCharts(){
        $this->json('GET', 'api/chart', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "code",
                "message",
                "data" => [
                    ["_id",
                    "product_id",
                    "name",
                    "price",
                    "qty",
                    "total",
                    "updated_at",
                    "created_at"]
                ]
            ]);
    }

    function testCheckoutProductOnCharts() {
        $data = [
            'customer_name' => 'Test Ihsan',
            'phone_number' => '621234567889',
            'address' => 'Bandung'
        ];
        $this->json('POST', 'api/chart/checkout', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "code",
                "message",
                "data" =>[
                    "customer_name",
                    'phone_number',
                    'address',
                    'total_prices',
                    "details" => [[
                        "_id",
                        "product_id",
                        "name",
                        "price",
                        "qty",
                        "total",
                        "updated_at",
                        "created_at"
                    ]]
                ]
                
            ]);
    }

    function testCheckoutEmptyProductOnCharts() {
        Chart::truncate();
        $data = [
            'customer_name' => 'Test Ihsan',
            'phone_number' => '621234567889',
            'address' => 'Bandung'
        ];
        $this->json('POST', 'api/chart/checkout', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "status" => false,
                "code" => "002",
                "message" => "chart is empty",
                "data" => null
            ]);
    }

    // function testCheckoutProductValidationOnCharts() {
    //     $data = [
    //         'customer_name' => 12345,
    //         'phone_number' => 12234,
    //         'address' => 1234
    //     ];
    //     $this->json('POST', 'api/chart/checkout', $data, ['Accept' => 'application/json'])
    //         ->assertStatus(200)
    //         ->assertJson([
    //             "status" => false,
    //             "code" => "422",
    //             "message" => "Validation failed",
    //             "data" => [
    //                 "customer_name" => ["The customer name must be a string."],
    //                 "phone_number" => ["The phone number must be at least 11 characters."],
    //                 "address" => ["The address must be a string"]
    //             ]
    //         ]);
    // }
}
