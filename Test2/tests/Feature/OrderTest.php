<?php

namespace Tests\Feature;

use App\Models\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

     public function testGetListOrders()
     {
         $this->json('GET', 'api/order', ['Accept' => 'application/json'])
             ->assertStatus(200)
             ->assertJsonStructure([
                 "status",
                 "code",
                 "message",
                 "data" => [[
                    "customer_name",
                    'phone_number',
                    'address',
                    'total_prices',
                    "details" => [[
                        "_id",
                        "product_id",
                        "name",
                        "price",
                        "qty",
                        "total",
                        "updated_at",
                        "created_at"
                    ]]
                 ]]
             ]);
     }
     
     public function testGetSummary()
     {
         $this->json('GET', 'api/order/summary', ['Accept' => 'application/json'])
             ->assertStatus(200)
             ->assertJsonStructure([
                 "status",
                 "code",
                 "message",
                 "data" => [
                    "amount" => [
                        "totalOrders",
                        "totalAmount",
                        "currentMonth"
                    ],
                    "totalProducts",
                    "popularProduct" => [
                        "product",
                        "totalQuantity",
                        "totalAmount"
                    ]
                 ]
             ]);
     }

    public function testGetListEmptyOrders()
    {
        Order::truncate();
        $this->json('GET', 'api/order', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "status" => false,
                "code" => "002",
                "message" => "orders is empty",
                "data" => null
            ]);
    }
}
