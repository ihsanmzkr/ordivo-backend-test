<?php

use App\Http\Controllers\Api\ChartController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'product'], function () {
    Route::get('', [ProductController::class, 'index']);
    Route::post('store', [ProductController::class, 'store']);
    Route::get('{id}', [ProductController::class, 'show']);
    Route::post('update/{id}', [ProductController::class, 'update']);
    Route::delete('delete/{id}', [ProductController::class, 'destroy']);
});

Route::group(['prefix' => 'chart'], function () {
    Route::get('', [ChartController::class, 'index']);
    Route::post('store', [ChartController::class, 'store']);
    Route::post('checkout', [ChartController::class, 'checkout']);
});

Route::group(['prefix' => 'order'], function () {
    Route::get('', [OrderController::class, 'index']);
    Route::get('summary', [OrderController::class, 'summary']);
});