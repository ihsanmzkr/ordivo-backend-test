<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Order extends Eloquent
{
    use HasFactory;

    protected $collection = 'orders';
    protected $primaryKey = '_id';
    protected $fillable = [
        'customer_name',
        'phone_number',
        'address',
        'total_prices',
        'details'
    ];
}
