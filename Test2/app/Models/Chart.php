<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Chart extends Eloquent
{
    use HasFactory;

    protected $collection = 'charts';
    protected $primaryKey = '_id';
    protected $fillable = [
        'product_id',
        'name',
        'price',
        'qty',
        'total',
    ];
}
