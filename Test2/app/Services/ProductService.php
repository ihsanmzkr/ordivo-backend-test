<?php
namespace App\Services;

use App\Http\Resources\ApiResponse;
use App\Models\Product;

class ProductService {
    function list() {
        $products = Product::get()->toArray();

        if (!$products) {
            return new ApiResponse(false, "002", 'empty', null);
        }

        return new ApiResponse(true, "000", 'success', $products);
    }

    function store($params) {
        $product = new Product();
        $product->name = $params['name'];
        $product->price = $params['price'];
        $product->qty = $params['qty'];
        $product->save();

        return new ApiResponse(true, "000", 'success', $product);
    }

    function detail($id)
    {
        $product = Product::find($id);
        if (!$product) {
            return new ApiResponse(false, "404", 'not found', null);
        }
        return new ApiResponse(true, "000", 'success', $product);
    }

    function update($params, $id)
    {
        $product = Product::find($id);
        if (!$product) {
            return new ApiResponse(false, "404", 'not found', null);
        }
        $product->name = $params['name'];
        $product->price = $params['price'];
        $product->qty = $params['qty'];
        $product->save();
        return new ApiResponse(true, "000", 'success', $product);
    }

    function destroy($id)
    {
        $product = Product::find($id);
        if (!$product) {
            return new ApiResponse(false, "404", 'not found', null);
        }
        $product->delete();
        return new ApiResponse(true, "000", 'success', $product);
    }
}