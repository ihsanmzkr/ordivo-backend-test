<?php
namespace App\Services;

use App\Http\Resources\ApiResponse;
use App\Models\Chart;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderService {

    function checkout($data) {

        $chart = Chart::get()->toArray();

        // Store the order
        $order = new Order();
        $order->customer_name = $data['customer_name'];
        $order->phone_number = $data['phone_number'];
        $order->address = $data['address'];
        $order->total_prices = $data['total_prices'];
        $order->details = $chart;
        $order->save();
        
        foreach ($chart as $key) {
            // update quantity product
            $product = Product::find($key['product_id']);
            $product->qty = $product->qty - $key['qty'];
            $product->save();
        }
        
        // empty chart
        Chart::truncate();
        return null;
    }

    function list($params) {
        $orders = new Order();
        if (isset($params['customer_name'])) {
            $orders = $orders->where('customer_name', $params['customer_name']);
        }
        if (isset($params['phone_number'])){
            $orders = $orders->where('phone_number', $params['phone_number']);
        }

        $orders = $orders->get()->toArray();
        
        if (!$orders) {
            return new ApiResponse(false, "002", 'orders is empty', null);
        }

        return new ApiResponse(true, "000", 'success', $orders);
    }

    function summary() {

        $currentMonth = Carbon::now()->startOfMonth();
        $nextMonth = Carbon::now()->startOfMonth()->addMonth();

        $mostPopularProduct = DB::collection('orders')
        ->raw(function($collection) {
            return $collection->aggregate([
                [
                    '$match' => [
                        'details.product_id' => ['$exists' => true, '$ne' => null]
                    ]
                ],
                [
                    '$unwind' => '$details'
                ],
                [
                    '$group' => [
                        '_id' => '$details.product_id',
                        'totalQuantity' => ['$sum' => '$details.qty'],
                        'totalAmount' => ['$sum' => '$details.total']
                    ]
                ],
                [
                    '$sort' => [
                        'totalQuantity' => -1
                    ]
                ],
                [
                    '$limit' => 1
                ]
            ]);
        });

        $totalAmount = DB::collection('orders')
        ->where('created_at', '>=', $currentMonth)
        ->where('created_at', '<', $nextMonth)
        ->sum('total_prices');
        
        $totalOrders = DB::collection('orders')
        ->where('created_at', '>=', $currentMonth)
        ->where('created_at', '<', $nextMonth)
        ->count();

        $totalProducts = DB::collection('products')->count();

        $mostPopularProductArray = iterator_to_array($mostPopularProduct);

        if (!empty($mostPopularProductArray)) {
            $mostPopularProduct = $mostPopularProductArray[0];
        
            $productId = $mostPopularProduct->_id;
        
            // Retrieve the product details
            $product = Product::find($productId);
        
            // Prepare the JSON response
            $response = [
                "amount" => [
                    "totalOrders" => $totalOrders,
                    "totalAmount" => $totalAmount,
                    "currentMonth" => $currentMonth->format('F Y')
                ],
                "totalProducts" => $totalProducts,        
                "popularProduct" => [
                    'product' => $product->name,
                    'totalQuantity' => $mostPopularProduct->totalQuantity,
                    'totalAmount' => $mostPopularProduct->totalAmount
                ]
            ];
        
            // Output the result
            return new ApiResponse(true, "000", 'success', $response);
        } else {
            return new ApiResponse(false, "002", 'no order found', null);
        }

    }
}