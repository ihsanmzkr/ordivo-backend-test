<?php
namespace App\Services;

use App\Http\Resources\ApiResponse;
use App\Jobs\CheckoutJob;
use App\Models\Chart;
use App\Models\Product;

class ChartService {
    public function list()
    {
        $chart = Chart::get()->toArray();

        if (!$chart) {
            return new ApiResponse(false, "002", 'chart is empty', null);
        }

        return new ApiResponse(true, "000", 'success', $chart);
    }

    public function store($params)
    {
        // Validate the request
        $product_id = $params['product_id'];
        $product = Product::find($product_id);
        if (!$product) {
            return new ApiResponse(false, "404", 'product not found', null);
        }

        // check if the product is already in storage
        $check = Chart::where('product_id', $product_id)->first();
        if ($check) {
            // update quantity and total price
            $update = Chart::find($check->id);
            $update->qty = $check->qty + $params['qty'];
            $update->total = $update->qty * $product->price;
            $update->save();

            return new ApiResponse(true, "000", 'success', $update);
        }

        // store new product to chart
        $chart = new Chart();
        $chart->product_id = $product->id;
        $chart->name = $product->name;
        $chart->price = $product->price;
        $chart->qty = $params['qty'];
        $chart->total = $params['qty'] * $product->price;
        $chart->save();

        return new ApiResponse(true, "000", 'success', $chart);
    }

    public function checkout($params) {
        // check if the chart is not empty
        $chart = Chart::get()->toArray();
        if (!$chart) {
            return new ApiResponse(false, "002", 'chart is empty', null);
        }

        // count total prices
        $total_prices = 0;
        foreach ($chart as $key) {
            $total_prices = $total_prices + $key['total'];
        }

        // response data to frontend
        $response = [
            'customer_name' => $params['customer_name'],
            'phone_number' => $params['phone_number'],
            'address' => $params['address'],
            'total_prices' => $total_prices,
            'details' => $chart
        ];

        $data = $params;
        $data['total_prices'] = $total_prices;

        // insert data to queue
        \dispatch(new CheckoutJob($data));

        return new ApiResponse(true, "000", 'success', $response);
    }
}