<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ApiResponse;
use App\Services\ChartService;
use Illuminate\Support\Facades\Validator;

class ChartController extends Controller
{
    protected $chartService;

    public function __construct(ChartService $chartService) {
        $this->chartService = $chartService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->chartService->list();
        return $list;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = $this->chartService->store($request->all());
        return $store;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request) {
        // Validate the request
        $validator = Validator::make($request->all(), [
            'customer_name' => 'required|string|max:100',
            'phone_number' => ['required', 'regex:/\62([ -]?\d+)+/', 'min:11', 'max:20'],
            'address' => 'required|string|max:100',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return new ApiResponse(false, "422", 'Validation failed', $validator->errors());
        }

        $checkout = $this->chartService->checkout($request->all());
        return $checkout;
    }

}
