<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\OrderService;

class OrderController extends Controller{

    function index(Request $request) {
        $orderService = new OrderService();
        $orderlist = $orderService->list($request->all());
        return $orderlist;
    }

    function summary(Request $request) {
        $orderService = new OrderService();
        $orderlist = $orderService->summary($request->all());
        return $orderlist;
    }
}